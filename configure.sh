#!/usr/bin/env bash

if [ $# -ne 1 ]; then
  echo "Usage: $0 [tags]"
  exit 1
fi

tags=$1

red() { echo -e "\033[00;31m$1\033[0m"; }

shift

ansible-playbook -u "$USER" -i "environments/" playbook.yml --tags "${tags}" -K $@
