#!/usr/bin/env bash

ansible-galaxy install --role-file requirements.yml --roles-path ./external_roles $@

